from enum import Enum
from loguru import logger


class Status(Enum):
    """!Статусы работы камеры:
    NEUTRAL = начальный статус камер,
    NORMAL = камера работает в нормальной режиме,
    INWORK = экран камеры переключен,
    ERROR = ошибка работы камеры.
    """
    NEUTRAL = 0
    NORMAL = 1
    INWORK = 2
    ERROR = 3


class Chamber:
    """!Класс камеры
    """
    def __init__(self, value, number):
        """
        Функция инициализации.

        @param value: Данные конкретной камеры из файла settings.json
        @param number: номер камеры
        """
        image_path = f"images/{number}/{number}.bmp"
        self.image_path = is_valid_image_path(image_path)
        self.name = is_valid_name(value['chamberName'])
        self.number = is_valid_number(number)
        self.url = is_valid_url(value['url'])
        self.template_type = is_valid_template_type(str(value['chamberType']))
        self.status = Status.NEUTRAL

def is_valid_image_path(image_path):
    if isinstance(image_path, str):
        return image_path
    else:
        raise ValueError("301 image_path is not a string")

def is_valid_name(name):
    if isinstance(name, str):
        return name
    else:
        raise ValueError("302 name is not a string")
    
def is_valid_number(number):
    if isinstance(number, str):
        return number
    else:
        raise ValueError("303 number is not a string")

def is_valid_url(url):
    if isinstance(url, str):
        return url
    else:
        raise ValueError("304 url is not a string")

def is_valid_template_type(template_type):
    if isinstance(template_type, str):
        return template_type
    else:
        raise ValueError("305 template_type is not a string")
    

class ChambersPool:
    """
    Класс набора всех камер из файла settings.json.
    """
    def __init__(self):
        super(ChambersPool, self).__init__()
        self.pool = []

    def set_chambers_pool(self, chambers):
        """
        Функция инициализации набора камер.

        :param chambers: Данные всех камер из файла settings.json
        """
        try:
            for key, value in chambers.items():
                self.pool.append(Chamber(value=value, number=key))
        except Exception as e:
            logger.error(f"Error 306 when setting chambers pool | {e}")

    # def set_working_chambers_pool(self, arguments):
    #     for number in arguments:
    #         for chamber in self.pool:
    #             if number == chamber.number:
    #                 self.working_chambers_pool.append(chamber)
