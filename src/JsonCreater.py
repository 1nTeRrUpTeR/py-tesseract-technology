import json
from loguru import logger

settings_data = {"chambersPool": {
                    "Chamber1": {
                        "url": "",
                        "chamberName": "",
                        "chamberType": "ВК-250"
                    },
                    "Chamber2": {
                        "url": "",
                        "chamberName": "",
                        "chamberType": "ВК-1000"
                    },
                    "Chamber3": {
                        "url": "",
                        "chamberName": "",
                        "chamberType": "КТХВ-1000"
                    },
                    "Chamber4": {
                        "url": "",
                        "chamberName": "",
                        "chamberType": "ВК-250-K"
                    }
                }
            }

templates_data = {
                "templatesPool": {
                    "TemplateType":
                    {
                            "date": [],
                            "time": [],
                            "current_vacuum_value": [],
                            "set_vacuum_value": [],
                            "current_temperature": [],
                            "set_temperature": [],
                            "pixels": {
                                "fore_pump": [],
                                "turbo_pump": [],
                                "temp_regul": [],
                                "pres_regul": [],
                                "humi_regul": []}
                    },
                }
            }


def create_settings_json():
    """
    Функция для создания шаблона settings.json, если при запуске он отсутствует.
    """
    try:
        with open("configs/settings.json", "w", encoding="utf-8") as file:
            try:
                json.dump(settings_data, file, indent='\t', ensure_ascii=False)
            except Exception as e:
                logger.error(f"Error 401 when writing data to 'settings.json' | {e}")
    except Exception as e:
        logger.error(f"Error 402 creating 'settings.json' | {e}")


def create_templates_json():
    """
    Функция для создания шаблона templates.json, если при запуске он отсутствует.
    """
    try:
        with open("configs/templates.json", "w", encoding="utf-8") as file:
            try:
                json.dump(templates_data, file, indent='\t', ensure_ascii=False)
            except Exception as e:
                logger.error(f"Error 403 when writing data to 'templates.json' | {e}")
    except Exception as e:
        logger.error(f"Error 404 creating 'templates.json' | {e}")
