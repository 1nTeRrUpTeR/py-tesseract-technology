import requests
import time
import json
from chamber import ChambersPool, Status
from JsonCreater import create_settings_json, create_templates_json
from loguru import logger
from workflow import (
    ocr_an_image,
    open_an_image,
    set_last_time_dict,
    get_time_data_from_website,
)
import threading

"""
Создание логгера и 2 файлов, для вывода в них логов и данных камер. data.log для записи данных с камеры, смена файла раз в неделю,
удаление спустя месяц. {time:DD_MM_YY}.log для вывода всех данных c камер и логов.
"""
def init():
    templates = {}
    chambers_pool = ChambersPool()
    logger.remove(0)
    logger.add(
        "logs/{time:DD_MM_YY}.log",
        format="{time:DD_MM_YY at HH:mm:ss} | {level} | {message} | {module} | {line} | {exception}",
        rotation="00:00",
        retention="1 month",
    )
    logger.add(
        "logs/data.log",
        format="{message}",
        rotation="1 week",
        retention="1 month",
        filter=lambda record: record["level"].name == "INFO"
    )

    try:
        with open("configs/settings.json", "r", encoding="utf-8") as file:
            try:
                chambers = json.load(file)["chambersPool"]
                if chambers == []:
                    logger.error("Error 109 'settings.json' is empty")
            except Exception as e:
                logger.error(
                    f"Error 110 when reading 'settings.json' with parametr 'chambersPool' | {e}"
                )
            chambers_pool.set_chambers_pool(chambers)
            set_last_time_dict(chambers)
    except Exception:
        logger.error("Error 111 File 'settings.json' not found. File template will be created")
        create_settings_json()

    try:
        with open("configs/templates.json", "r", encoding="utf-8") as file:
            try:
                templates = json.load(file)["templatesPool"]
                if templates == []:
                    logger.error("Error 112 'templates.json' is empty")
            except Exception as e:
                logger.error(
                    f"Error 113 when reading 'templates.json' with parametr 'templatesPool' | {e}"
                )
    except Exception:
        logger.error("Error 114 File 'templates.json' not found. File template will be created")
        create_templates_json()
    return chambers_pool, templates

def get_data(chamber, templates):
    """
    Функция получения данных с изображения.

    @param chamber: Камера, с которой происходит работа в данный момент.
    """
    open_an_image(camera_number=chamber.number)
    ocr_an_image(chamber=chamber, templates=templates)


def get_image_from_website(chamber, event):
    ## Функция получения данных с изображения.

    # @param chamber: Камера, с которой происходит работа в данный момент.
    # @param event: Событие, состояние которого необходимо отслеживать, для остановки потока
    ##
    try:
        request = requests.get(chamber.url, timeout=100)
        if request.status_code == 200:
            if request.content:
                try:
                    output = open(chamber.image_path, "wb")
                    output.write(request.content)
                    output.close()
                    chamber.status = Status.NORMAL
                    event.set()
                except Exception as e:
                    logger.error(
                        f"Error 106 when open or write image to '{chamber.image_path}' file | {e}"
                    )
            else:
                logger.error("Error 107 when reading image from website (image is empty)")
                event.clear()
        else:
            chamber.status = Status.ERROR
            logger.error("Error 108 connecting to the website")
            event.clear()
    except Exception as e:
        chamber.status = Status.ERROR
        logger.warning(f"Chamber :{chamber.name} recently not callable | {e}")


def get_data_from_website(chamber, templates):
    """Функция получения данных с изображения.
    param chamber  Камера, с которой происходит работа в данный момент
    """
    try:
        while True:
            event = threading.Event()
            get_image_from_website(chamber, event,)
            if event.is_set():
                get_data(chamber, templates)
            else:
                try:
                    dt_dict = {}
                    filename = f"output/{chamber.number}.json"
                    if chamber.status == Status.INWORK:
                        current_date, current_time = get_time_data_from_website(
                            chamber.number
                        )
                        dt_dict["datefromweb"] = current_date
                        dt_dict["timefromweb"] = current_time
                        dt_dict["status"] = str(chamber.status)
                    elif chamber.status == Status.ERROR:
                        dt_dict["status"] = str(chamber.status)
                    with open(filename, "w") as file:
                        try:
                            json.dump(dt_dict, file, indent="\t")
                        except Exception as e:
                            logger.error(
                                f"Error 103 when writing data to '{filename}' | {e}"
                            )
                except Exception as e:
                    logger.error(f"Error 104 when opening file '{filename}' | {e}")
            time.sleep(600)
    except Exception as e:
        logger.error(
            f"Error 105 in the process of calling functions in infinite loop | {e}"
        )


def main():
    """
    Функция запуска потока для каждой камеры.
    """
    logger.info("Start")
    chambers_pool, templates = init()
    try:
        threads = []
        try:
            for chamber in chambers_pool.pool:
                threads.append(threading.Thread(target=get_data_from_website, args=(chamber, templates,), name=str(chamber.number),))
            # threads.append(target=bot.polling(non_stop=True))
        except Exception as e:
            logger.error(f"Error 101 when creating threads | {e}")
        for thread in threads:
            thread.start()
    except Exception as e:
        logger.error(f"Error 102 when starting threads | {e}")


if __name__ == "__main__":
    main()
