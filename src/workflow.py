import cv2
from chamber import Status
from PIL import Image, ImageEnhance
from datetime import datetime
import pytesseract
from loguru import logger
import json
import requests
from regexp import checking_time, checking_date, checking_value

"""
Списки допустимых символов для упрощения распознавания текста.
"""
date_allow_list = "-0123456789"
time_allow_list = ":0123456789"
value_allow_list = ".-0123456789"
date = datetime.now()
last_time = {}


def set_last_time_dict(chambers):
    """
    Функция инициализации массива последних значение времени для каждой камеры, для проверки зависания камеры по времени.

    :param chambers: Данные всех камер из файла settings.json
    """
    for key in chambers.keys():
        last_time[key] = ""


def open_an_image(camera_number):
    """
    Функция открывает изображение из соответствующей дириктории,
    масштабирует картинку и загружает ее по новому пути.

    @param camera_number: Номер камеры, с которой происходит работа в данный момент
    """
    try:
        image_path = f"images/{camera_number}/{camera_number}.bmp"
        image = Image.open(image_path)
        image_new_path = f"images/{camera_number}/{camera_number}.jpg"
        try:
            w, h = image.size
            image = image.resize((int(w * 1.3), int(h)))
        except Exception as e:
            logger.error(f"Error 201 when resizing image '{image_path}' | {e}")
        try:
            image.save(image_new_path, quality=100)
        except Exception as e:
            logger.error(f"Error 202 when saving resize image to '{image_new_path}' | {e}")
    except Exception as e:
        logger.error(f"Error 203 when opening '{image_path}' | {e}")


def preprocessig_the_time_image(path):
    """
    Фукнция производит предварительную обработку изображений времени и даты.

    @param path: Путь изображения
    """
    try:
        image = cv2.imread(path)
        # Вторым параметром передается размер "кисти".
        # На мой взгляд самым оптимальным является (5,5)
        try:
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
            image = cv2.erode(image, kernel, iterations=1)
            image = cv2.medianBlur(image, 9)
        except Exception as e:
            logger.error(f"Error 204 when preprocessing '{path}' with opencv | {e}")
        try:
            cv2.imwrite(path, image)
        except Exception as e:
            logger.error(
                f"Error 205 when saving preprocessed with opencv image to'{path}' | {e}"
            )
        try:
            image = Image.open(path)
        except Exception as e:
            logger.error(f"Error 206 when opening '{path}' with Pillow | {e}")
        try:
            enhancer = ImageEnhance.Contrast(image)
            image = enhancer.enhance(11)

            def fn(x):
                return 255 if x > 200 else 0

            # Преобразуем в черно-белый рисунок:
            result = image.convert("L").point(fn, mode="1")
        except Exception as e:
            logger.error(f"Error 207 when preprocessing '{path}' with pillow | {e}")
        try:
            result.save(path)
        except Exception as e:
            logger.error(
                f"Error 208 when saving preprocessed with pillow image to'{path}' | {e}"
            )
    except Exception as e:
        logger.error(f"Error 209 when opening '{path}' with opencv | {e}")


def preprocessig_the_value_image(path):
    """
    Фукнция производит предварительную обработку изображений числовых значений.

    @param path: Путь изображения
    """
    try:
        image = Image.open(path)
        try:
            enhancer = ImageEnhance.Contrast(image)
            image = enhancer.enhance(11)

            def fn(x):
                return 255 if x > 200 else 0

            # Преобразуем в черно-белый рисунок:
            result = image.convert("L").point(fn, mode="1")
        except Exception as e:
            logger.error(f"Error 210 when preprocessing '{path}' with Pillow | {e}")
        try:
            result.save(path)
        except Exception as e:
            logger.error(
                f"Error 211 when saving preprocessed with pillow image to'{path}' | {e}"
            )
    except Exception as e:
        logger.error(f"Error 212 when opening '{path}' with pillow | {e}")


def ocr_an_image(chamber, templates):
    """
    Данный метод осуществляет распознавание текста.

    @param chamber: Камера, с которой происходит работа в данный момент
    @param templates: Все имеющиеся шаблоны камер из файла templates.json
    """
    global last_time
    try:
        image_new_path = f"images/{chamber.number}/{chamber.number}.jpg"
        try:
            image = Image.open(image_new_path)
        except Exception as e:
            logger.error(f"Error 213 when opening '{image_new_path}' for OCR | {e}")
        data_dict = {}
        inwork_dict = {}
        data_dict["number"] = chamber.name
        inwork_dict["number"] = chamber.name
        try:
            current_date, current_time = get_time_data_from_website(chamber.number)
            data_dict["datefromweb"] = current_date
            data_dict["timefromweb"] = current_time
        except Exception as e:
            logger.error(f"Error 214 Get date and time from website error | {e}")
        try:
            for key, value in templates[chamber.template_type].items():
                if key != "pixels":
                    try:
                        tmp_image = image.crop(value)
                    except Exception as e:
                        logger.error(
                            f"Error 215 when cropping '{image_new_path}' for OCR | {e}"
                        )
                    try:
                        if key == "date" or key == "time":
                            w, h = tmp_image.size
                            tmp_image = tmp_image.resize((int(w * 4), int(h * 3)))
                        else:
                            w, h = tmp_image.size
                            tmp_image = tmp_image.resize((int(w * 2), int(h * 2)))
                        tmp_image.save(
                            f"images/{chamber.number}/{key}.jpg", quality=100
                        )
                    except Exception as e:
                        logger.error(
                            f"Error 216 when resizing and saving resize image for OCR | {e}"
                        )
                    try:
                        preprocessig_the_value_image(
                            path=f"images/{chamber.number}/{key}.jpg"
                        )
                        tesseract_data = pytesseract.image_to_string(
                            f"images/{chamber.number}/{key}.jpg",
                            config="--psm 6 --oem 3 -c tesseract_char_whitelist=0123456789.-",
                        ).strip()
                        if key == "date":
                            current_date = checking_date(tesseract_data)
                            data_dict[key] = current_date
                        elif key == "time":
                            current_time = tesseract_data
                            if last_time[chamber.number] != data_dict["timefromweb"]:
                                last_time[chamber.number] = data_dict["timefromweb"]
                                data_dict[key] = checking_time(current_time)
                            else:
                                chamber.status = Status.ERROR
                                logger.error(
                                    f"Chamber №{chamber.number} stopped in  {last_time}"
                                )
                        else:
                            try:
                                current_value = checking_value(tesseract_data, chamber)
                                if (
                                    key == "current_vacuum_value"
                                    and chamber.status != Status.INWORK
                                ):
                                    current_value = format(float(current_value), ".1e")
                                data_dict[key] = current_value
                            except Exception as e:
                                logger.warning(f"Get value from image error | {e}")
                                chamber.status = Status.INWORK
                    except Exception as e:
                        logger.error(f"Error 217 when OCR image | {e}")
                        chamber.status = Status.INWORK
                else:
                    get_pixels_from_image(
                        chamber, data_dict, templates[chamber.template_type]["pixels"]
                    )
        except Exception as e:
            chamber.status = Status.INWORK
            logger.error(e)
        data_dict["status"] = str(chamber.status)
        try:
            filename = f"output/{chamber.number}.json"
            with open(filename, "w") as file:
                if chamber.status == Status.NORMAL:
                    try:
                        json.dump(data_dict, file, indent="\t")
                        logger.error(f"{chamber.number} | {data_dict} ")
                    except Exception as e:
                        logger.error(f"Error 218 when writing data to '{filename}' | {e}")
                else:
                    current_date, current_time = get_time_data_from_website(
                        chamber.number
                    )
                    inwork_dict["datefromweb"] = current_date
                    inwork_dict["timefromweb"] = current_time
                    inwork_dict["status"] = str(chamber.status)
                    json.dump(inwork_dict, file, indent="\t")
                    logger.info(f"{chamber.number} | {inwork_dict} ")
        except Exception as e:
            logger.error(f"Error 219 when opening file '{filename}' | {e}")
    except Exception as e:
        logger.error(f"Error 220 in function for ocr an image {chamber.number} | {e}")


def get_time_data_from_website(number):
    """
    Функция получения даты и времени в сайта.

    @param number: Номер камеры, с которой происходит работа в данный момент
    """
    url = "http://192.168.101." + number + "/date.txt*"
    try:
        request = requests.get(url, timeout=100)
    except Exception as e:
        logger.error(f"Error 221 connecting to the website (time out) | {e}")
    try:
        string = request.text.replace("Latest Captured:  ", "")
        string = datetime.strptime(string, "%b %d, %Y %H:%M:%S")
        string = datetime.strftime(string, "%d-%m-%y %H:%M:%S")
        date, time = string.split()
    except Exception as e:
        logger.error(f"Error 222 in process parse of string with date and time | {e}")
    return date, time


def get_pixels_from_image(chamber, data_dict, pixels):
    """
    Функция распознавания состояния насосов и регуляторов камеры.

    @param chamber: Камера, с которой происходит работа в данный момент
    @param data_dict: Словарь, в который записываются все данные камеры с картинки
    @param pixels: Координаты пикселей агрегатов на изображении
    """
    filename = f"images/{chamber.number}/{chamber.number}.bmp"
    image = Image.open(filename)
    image_data = image.load()
    for key, value in pixels.items():
        x, y = value
        image_data[x, y]
        if chamber.status == Status.NORMAL:
            if image_data[x, y][0] < image_data[x, y][1]:
                data_dict[key] = True
            else:
                data_dict[key] = False
