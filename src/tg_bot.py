from telebot import *
import ast
from datetime import datetime


date = datetime.now()
apihelper.proxy = {
    'http': '192.168.101.236:3129',
    'https': '192.168.101.236:3129'
}
token = '6491882749:AAHy5cuqN5ZMhNtbMY8j_bT19zCJGYRPgdM'
bot = telebot.TeleBot(token)
chat = -960086526
user = 1291441143
f_read = open("data.log", "r")
data_row_count = len([row.rstrip() for row in f_read])
f_read.seek(0)
filename = date.strftime("%d_%m_%y") + ".log"
log_file = open(filename, "r")
log_row_count = len([row.rstrip() for row in log_file])
log_file.seek(0)


def start():
    global data_row_count, log_row_count
    f_read = open("data.log", "r")
    current_row_count = len([row.rstrip() for row in f_read])
    f_read.seek(0)
    if data_row_count != current_row_count:
        diff = current_row_count - data_row_count
        data_row_count = current_row_count
        last_lines = f_read.readlines()[data_row_count-diff:]
        for line in last_lines:
            if "Start" not in line:
                pos = line.find('{')
                string = line[pos:]
                string = string.replace("'", '"')
                dict = ast.literal_eval(string)
                number = dict["number"]
                set_temp = float(dict["set_temperature"])
                cur_temp = float(dict["current_temperature"])
                if set_temp + 5 < cur_temp or cur_temp < set_temp - 5:
                    if set_temp + 5 < cur_temp:
                        message = f"Внимание: температура в камере {number} больше установленной!\nУстановленная температура : {set_temp} < Текущая температура : {cur_temp}"
                    else:
                        message = f"Внимание: температура в камере {number} меньше установленной!\nУстановленная температура : {set_temp} > Текущая температура : {cur_temp}"
                    bot.send_message(chat, message)
    filename = date.strftime("%d_%m_%y") + ".log"
    log_file = open(filename, "r")
    current_log_row_count = len([row.rstrip() for row in log_file])
    log_file.seek(0)
    if log_row_count != current_log_row_count:
        diff = current_log_row_count - log_row_count
        log_row_count = current_log_row_count
        last_log_lines = log_file.readlines()[log_row_count-diff:]
        for line in last_log_lines:
            if "ERROR" in line and "Start" not in line:
                bot.send_message(chat, line)


while True:
    start()
