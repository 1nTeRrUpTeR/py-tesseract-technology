import psycopg2
    

def create_table():
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='1234'")
    cursor = conn.cursor()
    conn.autocommit = True
    sql = '''select exists(SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower('tesseract_technology'))'''

    cursor.execute(sql)
    checktruth = cursor.fetchall()
    truthchecked = checktruth[0]
    checked = str(truthchecked[0])
    if checked == "False":
        sql = '''CREATE database tesseract_technology ''';
        cursor.execute(sql)
        cursor.close() 
        conn.commit()
        conn.close()
    conn = psycopg2.connect("dbname='tesseract_technology' user='postgres' host='localhost' password='1234'")
    cursor = conn.cursor()
    cursor.execute("select exists(select * from information_schema.tables where table_name=%s)", ('tesseract_data',))
    exists = cursor.fetchone()[0]

    if not exists:
        print("Create db")
        command = (
            """
            CREATE TABLE tesseract_data (
                id SERIAL PRIMARY KEY NOT NULL,
                title VARCHAR(50) NOT NULL,
                status VARCHAR(50) NOT NULL,
                chamber_type VARCHAR(50) NOT NULL,
                date VARCHAR(50) NOT NULL,
                time VARCHAR(50) NOT NULL,
                current_vacuum NUMERIC,
                set_vacuum NUMERIC,
                current_temperature NUMERIC,
                set_temperature NUMERIC
            )
            """
        )
        try:
            cursor.execute(command)  
            cursor.close() 
            conn.commit() 
        except (Exception, psycopg2.DatabaseError) as error: 
            print(error) 
        finally: 
            if conn is not None: 
                conn.close()


def insert_data(all_data):
    for data in all_data:
        data_for_post = {
                "title": data["chamberName"],
                "status": data["status"],
                "chamber_type": data["chamberType"],
                "date": data["datafromweb"],
                "time": data["timefromweb"],
            }
        if data["status"] == "Status.NORMAL":
            if "current_vacuum_value" in data:
                data_for_post["current_vacuum"]= data["current_vacuum_value"]
            if "set_vacuum_value" in data:
                data_for_post["set_vacuum"]= data["set_vacuum_value"]
            if "current_temperature" in data:
                data_for_post["current_temperature"]= data["current_temperature"]
            if "set_temperature" in data:
                data_for_post["set_temperature"]= data["set_temperature"]

        conn = psycopg2.connect("dbname='tesseract_technology' user='postgres' host='localhost' password='1234'")
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT title FROM tesseract_data WHERE title = %s", (data["chamberName"],))
            if cursor.fetchone():
                sql = 'DELETE FROM tesseract_data WHERE title = %s'
                cursor.execute(sql, (data["chamberName"],))
                print("delete row")
        except: 
            pass
        else:
            sql = '''
                INSERT INTO %s (%s)
                VALUES (%%(%s)s );
                '''   % ("tesseract_data", ',  '.join(data_for_post),  ')s, %('.join(data_for_post))
            cursor.execute(sql, data_for_post)
        cursor.close() 
        conn.commit() 
        conn.close()


if __name__ == '__main__':
    data =  [{
        "chamberName": "T070N53001",
        "status": "Status.NORMAL",
        "chamberType": "ВК-250",
        "datafromweb": "18-07-23",
        "timefromweb": "17:31:01",
        "current_vacuum_value": "12",
        "set_vacuum_value": "12",
        "current_temperature": "10",
        "set_temperature": "10",
    },
    {
        "chamberName": "T070N49001",
        "status": "Status.NORMAL",
        "chamberType": "ВК-250",
        "datafromweb": "18-07-23",
        "timefromweb": "17:31:01",
        "current_vacuum_value": "12",
        "set_vacuum_value": "12",
        "current_temperature": "10",
        "set_temperature": "10",
    }]
    create_table()
    insert_data(data)