import re
from datetime import datetime
from chamber import Status

date = datetime.now()


def checking_time(value):
    """
    Функция для проверки корректности считывания времени.

    @param value: Время, считанное с дисплея камеры
    """
    regex = r"([01]\d|2[0-3]):[0-5]\d(:[0-5]\d)?"
    if re.fullmatch(regex, value):
        return value
    elif len(value) != 8:
        if value.count(":") == 2:
            symbol_pos = value.find(":")
            if symbol_pos == 2:
                symbol_second_pos = value.find(":", symbol_pos + 1, len(value))
                if symbol_second_pos == 5:
                    value = value[: symbol_second_pos + 1] + date.strftime("%S")
                    return value
                elif symbol_second_pos != 5:
                    value = (
                        value[: symbol_pos + 1]
                        + date.strftime("%M")
                        + value[symbol_second_pos:]
                    )
                    return value
            if symbol_pos < 2:
                value = date.strftime("%H") + value[symbol_pos:]
                return value
        elif value.count(":") == 1:
            symbol_pos = value.find(":")
            if len(value[symbol_pos + 1:]) >= 3:
                if len(value[:symbol_pos]) == 2:
                    value = value[:5] + ":" + value[5:]
                else:
                    value = date.strftime("%H") + value[symbol_pos:5] + ":" + value[5:]
                return value
            elif len(value[:symbol_pos]) >= 3:
                if len(value[symbol_pos + 1:]) == 2:
                    value = value[:2] + ":" + value[2:]
                else:
                    value = (
                        value[:2]
                        + ":"
                        + value[2: symbol_pos + 1]
                        + date.strftime("%S")
                    )
                return value
        else:
            value = value[:2] + ":" + value[2:4] + ":" + value[4:]


def checking_date(value):
    """
    Функция для проверки корректности считывания даты.

    @param value: Дата, считанное с дисплея камеры
    """
    regex = r"(0[1-9]|[12][0-9]|3[01])[-](0?[1-9]|1[012])[-][0-9]{2}"
    if re.fullmatch(regex, value):
        return value
    else:
        return date.strftime("%d-%m-%y")


def checking_value(value, chamber):
    """
    Функция для проверки корректности считывания всех числовых значений, в случае ошибки статус камеры переводится в INWORK.

    @param value: Числовое значение, считанное с дисплея камеры
    @param chamber: Камера, для которой происходит проверка
    """
    regex = r"(\-|\+)?\d+(\.\d+)?"
    if re.fullmatch(regex, value):
        return value
    elif value.count(" ") != 0:
        value = value.replace(" ", "")
        return value
    else:
        chamber.status = Status.INWORK
